import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"


const SecondPage = ({ data }) => (
  <Layout>
    <SEO title="masterdruglist" />
    <h1>Master Drug List</h1>
<p>Total Drugs: {data.postgres.allTblRefMasterDruglistsList.count}</p>
    <ul>
      {data.postgres.allTblRefMasterDruglistsList.map(drug => (
        <li>
          <strong>
            {drug.masterDrugIdent}-
            {drug.masterDrugName}
          </strong>
         
        </li>
      ))}
    </ul>
    <Link to="/">Go back to the homepage</Link>
  </Layout>
);

export const query = graphql`
  {
    postgres {
      allTblRefMasterDruglistsList{
        masterDrugIdent
        masterDrugName
      }
    }
  }
`;

export default SecondPage
